import { Component, OnInit } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Post } from '../Post';
import { Input, Output } from '@angular/core';
import { DatiService } from '../services/dati.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  @Input() post:Post;
  @Output() serverUpdate:EventEmitter<boolean>;

  addForm:FormGroup;

  hiddenId:boolean=true;

  constructor(private datiService:DatiService, private formBuilder:FormBuilder) {
    this.serverUpdate=new EventEmitter<boolean>();
    this.post=new Post();
    this.addForm=this.createFormGroup();
   }

  ngOnInit(): void {
    this.addForm=this.createFormGroup();

    this.hiddenId=(this.post.id==0);
  }

  createFormGroup(){
    return this.formBuilder.group({
      'id':[this.post.id],
      'title':[this.post.title],
      'author':[this.post.author],
      'content':[this.post.content]
    });
  }

  onSubmit(submittedPost:Post){
    if(submittedPost.id!=0){
      this.datiService.update(submittedPost).subscribe(res=>{
        this.serverUpdate.emit(true);
      });
    }
    else{
      this.datiService.add(submittedPost).subscribe(res=>{
        this.serverUpdate.emit(true);
      });
    }
  }

  cancel(){
    this.serverUpdate.emit(false);
  }
}
