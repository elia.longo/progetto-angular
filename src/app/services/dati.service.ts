import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '../Post';

@Injectable({
  providedIn: 'root'
})
export class DatiService {
  url:string="http://localhost:3000/posts";
  constructor(private http:HttpClient) { }

  getAll(): Observable<Post[]>{
    return this.http.get<Post[]>(this.url);
  }

  getSinglePost(id:number):Observable<Post>{
    return this.http.get<Post>(this.url+'/'+id);
  }

  add(post:Post){
    console.log(post);  
    return this.http.post(this.url, post);
  }

  canc(id:number){
    return this.http.delete(this.url+"/"+id);
  }

  update(post:Post){
    return this.http.put(this.url+"/"+post.id, post);
  }

}
