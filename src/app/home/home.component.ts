import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { DatiService } from '../services/dati.service';
import { Post } from '../Post';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  $posts?:Observable<Post[]>;
  
  isAdd:boolean=false;
  
  addPost:Post;

  constructor(private datiService:DatiService, private router:Router){
    this.addPost=new Post();
    this.$posts=datiService.getAll();
  }

  addToggle(){
    this.isAdd=!this.isAdd;
  }


  canc(id:number){
    this.datiService.canc(id).subscribe(res => {
      console.log(res);
      this.$posts=this.datiService.getAll();
    });
  }


  update(post:Post){
    this.addPost=post;
    this.isAdd=true;
  }

  onServerUpdate(success:boolean){
    this.addToggle();
    this.addPost=new Post();
    if (success)
      this.$posts=this.datiService.getAll();
  }
}
