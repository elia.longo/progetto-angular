import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Post } from '../Post';
import { DatiService } from '../services/dati.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  id:number=0;

  post:Post=new Post();

  constructor(private route:ActivatedRoute, private datiService:DatiService) {
    route.params.subscribe(params=>{this.id=params['id'];});    
  }  

  ngOnInit(): void {
    this.datiService.getSinglePost(this.id).subscribe(res=>{this.post=res});
  }

}
